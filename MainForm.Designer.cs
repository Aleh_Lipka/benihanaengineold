﻿namespace Benihana
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.renderTimer = new System.Windows.Forms.Timer(this.components);
            this.dxPanel = new Benihana.DXPanel();
            this.SuspendLayout();
            // 
            // renderTimer
            // 
            this.renderTimer.Enabled = true;
            this.renderTimer.Interval = 16;
            this.renderTimer.Tick += new System.EventHandler(this.Update);
            // 
            // dxPanel
            // 
            this.dxPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dxPanel.Location = new System.Drawing.Point(0, 0);
            this.dxPanel.MinimumSize = new System.Drawing.Size(1, 1);
            this.dxPanel.Name = "dxPanel";
            this.dxPanel.Size = new System.Drawing.Size(736, 440);
            this.dxPanel.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(736, 440);
            this.Controls.Add(this.dxPanel);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Benihana Engine";
            this.ResumeLayout(false);

        }

        #endregion

        private DXPanel dxPanel;
        private System.Windows.Forms.Timer renderTimer;
    }
}

