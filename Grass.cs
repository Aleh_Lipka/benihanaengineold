﻿using System.Collections.Generic;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using System.Drawing;
using System;
using Benihana.Parser.OBJ;
namespace Benihana
{
    class Grass
    {
        private Device device;
        private Bitmap HeightMap, GrassMap;
        private List<Vector3> PositionList;
        private Vector3 CameraPosition, CameraDirection;
        private OBJParser parser;
        public Grass(Device device, Bitmap HeightMap)
        {
            this.device = device;
            this.HeightMap = HeightMap;
            parser = new OBJParser(device);
            GrassMap = new Bitmap(@"Landscapes\Level1\Level1GM.png");
            Load();
        }
        public void ResetDevice(Device device)
        {
            this.device = device;
            parser.ResetDevice(this.device);
        }
        public void Load()
        {
            PositionList = new List<Vector3>();
            for (int z = 0; z < GrassMap.Height; z++)
            {
                for (int x = 0; x < GrassMap.Width; x++)
                {
                    if (GrassMap.GetPixel(x, z) == Color.FromArgb(0, 255, 0))
                    {
                        float y = HeightMap.GetPixel(x, z).R / 255.0f * 10;
                        PositionList.Add(new Vector3(x, y, z));
                    }
                }
            }
            parser.Read($"Models\\Terrains\\Grass_01.objmodel", 1);
        }
        public void Update(Vector3 Position, Vector3 Direction)
        {
            CameraPosition = Position;
            CameraDirection = Direction;
        }
        public void Draw()
        {
            Material material = new Material();
            material.Diffuse = Color.White;
            material.Ambient = Color.Gray;
            material.Specular = Color.White;
            device.Material = material;

            device.RenderState.CullMode = Cull.None;
            foreach (Vector3 position in PositionList)
            {
                Vector2 v2cam = new Vector2(CameraPosition.X, CameraPosition.Z);
                Vector2 v2grs = new Vector2(position.X, position.Z);
                Vector2 v2dist = v2cam - v2grs;
                if (v2dist.Length() < Program.DrawDistance)
                {
                    Vector2 v2dir = new Vector2(CameraDirection.X, CameraDirection.Z);
                    float viewAngle = Program.RadianToDegree(Program.AngleBeetwean(v2dir, v2dist));
                    if (viewAngle > 180 - 45 && viewAngle < 180 + 45) // возможно, вектор камеры направлен не в ту сторону
                    {
                        float grassAngle = Program.AngleBeetwean(new Vector2(0, 1), v2dist);
                        device.Transform.World = Matrix.Identity;
                        device.Transform.World *= Matrix.RotationY(grassAngle); // повернутая на камеру трава
                        device.Transform.World *= Matrix.Translation(position);
                        parser.Draw();
                    }
                }
            }
            device.Transform.World = Matrix.Identity;
            device.RenderState.CullMode = Cull.Clockwise;
        }
    }
}