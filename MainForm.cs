﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using System.Threading;
namespace Benihana
{
    enum MouseAction { MoveObject, MoveCamera, None };
    public partial class MainForm : Form
    {
        private Device device;
        private Camera camera;
        private PresentParameters present_params;
        private MouseAction mouseAction = MouseAction.None;
        private Vector2 prePosMouse;
        private Pokemon cubone;
        private Ground ground;
        private Grass grass;
        private SkyBox skybox;
        private Water water;
        private System.Drawing.Font system_font;
        private Microsoft.DirectX.Direct3D.Font system_text;
        private string system_string;
        public MainForm()
        {
            InitializeComponent();
            InitializeGraphics();
            
            cubone = new Pokemon(device, "Cubone");
            cubone.Load();
            cubone.Position = new Vector3(26, 0, 39);
            ground = new Ground(device);
            grass = new Grass(device, ground.HeightMap);
            camera = new Camera(device, dxPanel, new Vector3(26, 1.5f, 47));
            skybox = new SkyBox(device);
            water = new Water(device);

            system_font = new System.Drawing.Font("Arial", 8f, FontStyle.Bold);
            system_text = new Microsoft.DirectX.Direct3D.Font(device, system_font);

            dxPanel.MouseUp += DxPanel_MouseUp;
            dxPanel.MouseDown += DxPanel_MouseDown;
            dxPanel.MouseMove += DxPanel_MouseMove;
            dxPanel.MouseWheel += DxPanel_MouseWheel;

            KeyDown += MainForm_KeyDown;

            device.DeviceReset += Device_DeviceReset;
        }
        private void Device_DeviceReset(object sender, EventArgs e)
        {
            #region RESETUP DEVICE
            int adapterOrdinal = Manager.Adapters.Default.Adapter;
            CreateFlags flags = CreateFlags.SoftwareVertexProcessing;
            Caps caps = Manager.GetDeviceCaps(adapterOrdinal, DeviceType.Hardware);
            if (caps.DeviceCaps.SupportsHardwareTransformAndLight) { flags = CreateFlags.HardwareVertexProcessing; }
            if (caps.DeviceCaps.SupportsPureDevice) { flags |= CreateFlags.PureDevice; }
            device = new Device(0, DeviceType.Hardware, dxPanel, flags, present_params);
            #region Установка фильтрации текстуры
            // Установка фильтра сжатия - анизотропный, если поддерживается видеокартой, иначе - линейный (если тоже поддерживается)
            if (device.DeviceCaps.TextureFilterCaps.SupportsMinifyAnisotropic) { device.SamplerState[0].MinFilter = TextureFilter.Anisotropic; }
            else if (device.DeviceCaps.TextureFilterCaps.SupportsMinifyLinear) { device.SamplerState[0].MinFilter = TextureFilter.Linear; }
            // Установка фильтра растяжения - анизотропный, если поддерживается видеокартой, иначе - линейный (если тоже поддерживается)
            if (device.DeviceCaps.TextureFilterCaps.SupportsMagnifyAnisotropic) { device.SamplerState[0].MagFilter = TextureFilter.Anisotropic; }
            else if (device.DeviceCaps.TextureFilterCaps.SupportsMagnifyLinear) { device.SamplerState[0].MagFilter = TextureFilter.Linear; }
            // Выставляем линейное сглаживание текстур
            device.SetSamplerState(0, SamplerStageStates.MinFilter, (int)TextureFilter.Linear);
            device.SetSamplerState(0, SamplerStageStates.MagFilter, (int)TextureFilter.Linear);
            device.SetSamplerState(0, SamplerStageStates.MipFilter, (int)TextureFilter.Linear);
            #endregion
            #region Альфа-сглаживание
            device.RenderState.AlphaBlendEnable = true;
            device.RenderState.SourceBlend = Blend.SourceAlpha;
            device.RenderState.DestinationBlend = Blend.InvSourceAlpha;
            device.RenderState.BlendOperation = BlendOperation.Add;
            #endregion
            #region Туман
            device.RenderState.FogColor = Color.DarkGray;
            device.RenderState.FogStart = 15.0f;
            device.RenderState.FogEnd = 70.0f;
            device.RenderState.FogTableMode = FogMode.Linear;
            device.RenderState.FogEnable = true;
            #endregion
            #endregion
            system_text = new Microsoft.DirectX.Direct3D.Font(device, system_font);
            ground.ResetDevice(device);
            grass.ResetDevice(device);
            cubone.ResetDevice(device);
            camera.ResetDevice(device);
            skybox.ResetDevice(device);
            water.ResetDevice(device);
        }
        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) { Application.Exit(); }
            
            if (e.KeyCode == Keys.Z) { cubone.Emotion = Program.Emotion.A; }
            if (e.KeyCode == Keys.X) { cubone.Emotion = Program.Emotion.B; }
            if (e.KeyCode == Keys.C) { cubone.Emotion = Program.Emotion.C; }
            if (e.KeyCode == Keys.V) { cubone.Emotion = Program.Emotion.D; }
            if (e.KeyCode == Keys.B) { cubone.Emotion = Program.Emotion.E; }
            if (e.KeyCode == Keys.N) { cubone.Emotion = Program.Emotion.F; }
            if (e.KeyCode == Keys.M) { cubone.Emotion = Program.Emotion.G; }

            float step = 0.1f;
            if (e.KeyCode == Keys.A) { cubone.Position.X -= step; }
            if (e.KeyCode == Keys.D) { cubone.Position.X += step; }
            if (e.KeyCode == Keys.W) { cubone.Position.Z -= step; }
            if (e.KeyCode == Keys.S) { cubone.Position.Z += step; }
            
            if (e.KeyCode == Keys.Right) { ground.OffsetVector.X += 0.1f; }
            if (e.KeyCode == Keys.Left) { ground.OffsetVector.X -= 0.1f; }
            if (e.KeyCode == Keys.Up) { ground.OffsetVector.Z -= 0.1f; }
            if (e.KeyCode == Keys.Down) { ground.OffsetVector.Z += 0.1f; }
        }
        private void DxPanel_MouseWheel(object sender, MouseEventArgs e)
        {
            camera.SetPosition(camera.direction * 0.001f * e.Delta, true);
        }
        private void DxPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseAction == MouseAction.MoveCamera)
            {
                Vector2 mousePos = new Vector2(e.X, e.Y);
                Vector2 offset = mousePos - prePosMouse;
                float angleX = -offset.Y / 400;
                float angleY = -offset.X / 400;
                Vector3 up = camera.GetUpVector();
                Vector3 axis = Vector3.Cross(camera.direction, camera.GetUpVector());
                axis.Normalize();
                camera.direction.TransformCoordinate(Matrix.RotationAxis(axis, angleX));
                camera.direction.TransformCoordinate(Matrix.RotationAxis(up, angleY));
                camera.Update();
                prePosMouse = mousePos;
            }
        }
        private void DxPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                mouseAction = MouseAction.MoveCamera;
                prePosMouse = new Vector2(e.X, e.Y);
            }
        }
        private void DxPanel_MouseUp(object sender, MouseEventArgs e)
        {
            mouseAction = MouseAction.None;
        }
        private void InitializeGraphics()
        {
            #region Настройка девайса
            present_params = new PresentParameters();
            present_params.Windowed = true;
            present_params.SwapEffect = SwapEffect.Discard;
            present_params.EnableAutoDepthStencil = true;
            present_params.AutoDepthStencilFormat = DepthFormat.D16;
            present_params.BackBufferFormat = Manager.Adapters[0].CurrentDisplayMode.Format;
            present_params.BackBufferCount = 2;
            int adapterOrdinal = Manager.Adapters.Default.Adapter;
            CreateFlags flags = CreateFlags.SoftwareVertexProcessing;
            Caps caps = Manager.GetDeviceCaps(adapterOrdinal, DeviceType.Hardware);
            if (caps.DeviceCaps.SupportsHardwareTransformAndLight) { flags = CreateFlags.HardwareVertexProcessing; }
            if (caps.DeviceCaps.SupportsPureDevice) { flags |= CreateFlags.PureDevice; }
            if (device != null) device.Dispose();
            // Перебираем все режимы MSAA от MSAA 16x до MSAA 2x
            for (present_params.MultiSample = MultiSampleType.SixteenSamples; present_params.MultiSample >= MultiSampleType.TwoSamples; present_params.MultiSample--)
            {
                try
                {
                    // Пытаемся создать устройство с использованием текущего режима MSAA
                    device = new Device(0, DeviceType.Hardware, dxPanel, flags, present_params);
                    // Если удалось создать устройство, прерываем цикл
                    break;
                }
                catch (InvalidCallException) { }
            }
            // Если устройство всё же создать не удалось
            if (device == null)
            {
                // Выключаем MSAA
                present_params.MultiSample = MultiSampleType.None;
                // Создаём устройство
                device = new Device(0, DeviceType.Hardware, dxPanel, flags, present_params);
            }
            #endregion
            #region Установка фильтрации текстуры
            // Установка фильтра сжатия - анизотропный, если поддерживается видеокартой, иначе - линейный (если тоже поддерживается)
            if (device.DeviceCaps.TextureFilterCaps.SupportsMinifyAnisotropic) { device.SamplerState[0].MinFilter = TextureFilter.Anisotropic; }
            else if (device.DeviceCaps.TextureFilterCaps.SupportsMinifyLinear) { device.SamplerState[0].MinFilter = TextureFilter.Linear; }
            // Установка фильтра растяжения - анизотропный, если поддерживается видеокартой, иначе - линейный (если тоже поддерживается)
            if (device.DeviceCaps.TextureFilterCaps.SupportsMagnifyAnisotropic) { device.SamplerState[0].MagFilter = TextureFilter.Anisotropic; }
            else if (device.DeviceCaps.TextureFilterCaps.SupportsMagnifyLinear) { device.SamplerState[0].MagFilter = TextureFilter.Linear; }
            // Выставляем линейное сглаживание текстур
            device.SetSamplerState(0, SamplerStageStates.MinFilter, (int)TextureFilter.Linear);
            device.SetSamplerState(0, SamplerStageStates.MagFilter, (int)TextureFilter.Linear);
            device.SetSamplerState(0, SamplerStageStates.MipFilter, (int)TextureFilter.Linear);
            #endregion
            #region Альфа-сглаживание
            device.RenderState.AlphaBlendEnable = true;
            device.RenderState.SourceBlend = Blend.SourceAlpha;
            device.RenderState.DestinationBlend = Blend.InvSourceAlpha;
            device.RenderState.BlendOperation = BlendOperation.Add;
            #endregion
            #region Туман
            device.RenderState.FogColor = Color.DarkGray;
            device.RenderState.FogStart = 15.0f;
            device.RenderState.FogEnd = 70.0f;
            device.RenderState.FogTableMode = FogMode.Linear;
            device.RenderState.FogEnable = true;
            #endregion
        }
        private void SetupLighting()
        {
            device.Lights[0].Type = LightType.Directional;
            device.Lights[0].Diffuse = Color.White;
            device.Lights[0].Ambient = Color.White;
            device.Lights[0].Direction = new Vector3((float)Math.Cos(Environment.TickCount / (250.0f * 100)), 1.0f, (float)Math.Sin(Environment.TickCount / (250.0f * 100)));
            device.Lights[0].Update();
            device.Lights[0].Enabled = true;
        }
        private void Update(object sender, EventArgs e)
        {
            SetupLighting();
            cubone.Update(camera.GetPosition(), camera.direction);
            ground.Update(camera);
            skybox.Update(camera.GetPosition());
            grass.Update(camera.GetPosition(), camera.direction);
            water.Update(camera.GetPosition(), camera.direction);
            system_string =
                "Debug information:\n-------------------------------\n\n"
                + $"Camera position:\n{camera.GetPosition()}\n"
                + $"Camera direction:\n{camera.direction}\n"
                + $"Light position:\n{device.Lights[0].Position}\n"
                + $"Light direction:\n{device.Lights[0].Direction}\n"
                + $"Cubone position:\n{cubone.Position}\n"
                + $"Ground position:\n{ground.OffsetVector}";
            Draw();
        }
        private void Draw()
        {
            device.Clear(ClearFlags.Target | ClearFlags.ZBuffer, Color.Black, 1.0f, 0);
            device.BeginScene();
            skybox.Draw();
            cubone.Draw();
            ground.Draw();
            grass.Draw();
            water.Draw();
            system_text.DrawText(null, system_string, new Point(5, 5), Color.White);
            device.EndScene();
            device.Present();
        }
    }
}