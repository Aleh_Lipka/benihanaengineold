﻿using System;
namespace Benihana
{
    class Timer
    {
        public bool IsWorking = false;
        private int StartValue = 0;

        public void Start()
        {
            IsWorking = true;
            StartValue = Environment.TickCount;
        }

        public int Stop()
        {
            IsWorking = false;
            return Environment.TickCount - StartValue;
        }

        public int CurrentValue()
        {
            if (IsWorking) { return Environment.TickCount - StartValue; }
            return 0;
        }
    }
}
