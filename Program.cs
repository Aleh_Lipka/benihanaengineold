﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.DirectX;

namespace Benihana
{
    static class Program
    {
        public enum PokemonType { Electric, Whater, Fire, Wind, Ground }
        public enum Gender { Male, Female }
        public enum Emotion { A, B, C, D, E, F, G }

        public static int DrawDistance = 60;
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        static public float DegreeToRadian(float angle)
        {
            return (float)(Math.PI * angle / 180);
        }

        static public float RadianToDegree(float angle)
        {
            return (float)(angle * (180 / Math.PI));
        }

        static public float AngleBeetwean(Vector2 va, Vector2 vb)
        {
            // условие: совпадение концов векторов
            if ((va - vb).Length() < 0.001f)
            {
                return 0.0f;
            }
            // условие: один из векторов - нулевой
            if (va.Length() == 0 || vb.Length() == 0)
            {
                return 0.0f;
            }
            // находим угол между [1;0] и va (-180°..180°)
            double angle1 = Math.Acos(va.X / va.Length());
            if (va.Y < 0)
            {
                angle1 = -angle1;
            }
            // находим угол между [1;0] и vb (-180°..180°)
            double angle2 = Math.Acos(vb.X / vb.Length());
            if (vb.Y < 0)
            {
                angle2 = -angle2;
            }
            //находим угол между va и vb и если он отрицательный - добавляем 360°
            double dangle = angle1 - angle2;
            if (dangle < 0.0)
            {
                dangle += 2.0 * Math.PI;
            }
            return (float)dangle;
        }
    }
}
