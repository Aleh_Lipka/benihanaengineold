﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.DirectX;
using System.IO;
using System.Globalization;
using Microsoft.DirectX.Direct3D;
using System.Drawing;
using System.Text.RegularExpressions;

namespace Benihana.Parser.OBJ
{
    class OBJParser
    {
        struct MassiveWithTexture
        {
            public CustomVertex.PositionNormalTextured[] massive;
            public Texture texture;
            public string material;
        }

        public Matrix RotationMatrix;
        public string ObjFilePath;
        public string MtlFilePath;
        public float Emotion_x = 0.0f, Emotion_y = 0.0f;
        public float Scale = 1;

        private List<Vector3> VertexList;
        private List<Vector2> TextureList;
        private List<Vector3> NormalList;
        private List<string> GroupList;
        private List<string> MaterialList;
        private List<Face> FaceList;
        private List<MTL> MTL;
        private string FolderPath;
        private StreamReader objStreamReader;
        private StreamReader mtlStreamReader;
        private CultureInfo cultureInfo;
        private Device device;
        private List<MassiveWithTexture> MassiveObjectsList;
        private int PrimitivesCount = 3;

        public OBJParser(Device device)
        {
            cultureInfo = CultureInfo.CreateSpecificCulture("en-US");
            VertexList = new List<Vector3>();
            TextureList = new List<Vector2>();
            NormalList = new List<Vector3>();
            GroupList = new List<string>();
            MaterialList = new List<string>();
            FaceList = new List<Face>();
            MTL = new List<MTL>();

            this.device = device;
        }

        public void ClearAllLists()
        {
            VertexList.Clear();
            TextureList.Clear();
            NormalList.Clear();
            GroupList.Clear();
            MaterialList.Clear();
            FaceList.Clear();
            MTL.Clear();
        }

        public void Read(string obj_path, float scale)
        {
            ClearAllLists();
            if (scale == 0) scale = 1;
            Scale = scale;
            ObjFilePath = obj_path;
            ParseOBJ(ObjFilePath, scale);
            MtlFilePath = Path.ChangeExtension(ObjFilePath, ".mtl");
            FolderPath = Path.GetDirectoryName(MtlFilePath) + @"\";
            ParseMTL(MtlFilePath);
            CalculateVerts();
        }

        public void ResetDevice(Device device)
        {
            this.device = device;
            Read(ObjFilePath, Scale);
        }

        private void ParseOBJ(string path, float scale)
        {
            FileInfo fileInfo = new FileInfo(path);
            objStreamReader = fileInfo.OpenText();
            string currentLine = string.Empty;
            string currentMaterial = string.Empty;
            while ((currentLine = objStreamReader.ReadLine()) != null)
            {
                currentLine = new Regex("\\s+").Replace(currentLine, " ");
                string[] words = currentLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (words.Length > 0)
                {
                    bool success;
                    float x, y, z;
                    float v, t, n;
                    switch (words[0])
                    {
                        #region VERTEX
                        case "v":
                            success = float.TryParse(words[1], NumberStyles.Float, cultureInfo, out x);
                            if (!success) throw new ArgumentException("Невозможно считать X параметр вершины как float");
                            success = float.TryParse(words[2], NumberStyles.Float, cultureInfo, out y);
                            if (!success) throw new ArgumentException("Невозможно считать Y параметр вершины как float");
                            success = float.TryParse(words[3], NumberStyles.Float, cultureInfo, out z);
                            if (!success) throw new ArgumentException("Невозможно считать Z параметр вершины как float");
                            VertexList.Add(new Vector3(x / scale, y / scale, z / scale));
                            break;
                        #endregion
                        #region TEXTURE
                        case "vt":
                            success = float.TryParse(words[1], NumberStyles.Float, cultureInfo, out x);
                            if (!success) throw new ArgumentException("Невозможно считать X параметр текстуры как float");
                            success = float.TryParse(words[2], NumberStyles.Float, cultureInfo, out y);
                            if (!success) throw new ArgumentException("Невозможно считать Y параметр текстуры как float");
                            TextureList.Add(new Vector2(x, -y));
                            break;
                        #endregion
                        #region NORMAL
                        case "vn":
                            success = float.TryParse(words[1], NumberStyles.Float, cultureInfo, out x);
                            if (!success) throw new ArgumentException("Невозможно считать X параметр нормали как float");
                            success = float.TryParse(words[2], NumberStyles.Float, cultureInfo, out y);
                            if (!success) throw new ArgumentException("Невозможно считать Y параметр нормали как float");
                            success = float.TryParse(words[3], NumberStyles.Float, cultureInfo, out z);
                            if (!success) throw new ArgumentException("Невозможно считать Z параметр нормали как float");
                            NormalList.Add(new Vector3(x, y, z));
                            break;
                        #endregion
                        #region GROUP
                        case "g":
                            GroupList.Add(words[1]);
                            break;
                        #endregion
                        #region MATERIAL
                        case "usemtl":
                            MaterialList.Add(words[1]);
                            currentMaterial = words[1];
                            break;
                        #endregion
                        #region FACE
                        case "f":
                            // f 51/1/37 48/2/48 6/3/6 5/4/5
                            // 51 - порядковый номер вершины
                            // 1 - порядковый номер текстуры
                            // 37 - порядковый номер нормали
                            // Порядковые номера в OBJ отсчитываются от 1, а не от 0
                            Face face = new Face();
                            face.MaterialName = currentMaterial;
                            if (words.Count() == 5) { face.Type = FaceTypes.Rectangle; } else { face.Type = FaceTypes.Triangle; }
                            string[] parts = null;

                            parts = words[1].Split('/');
                            success = float.TryParse(parts[0], NumberStyles.Float, cultureInfo, out v);
                            if (!success) throw new ArgumentException("Невозможно считать вершинный параметр плоскости как float в первой группе");
                            success = float.TryParse(parts[1], NumberStyles.Float, cultureInfo, out t);
                            if (!success) throw new ArgumentException("Невозможно считать текстурный параметр плоскости как float в первой группе");
                            success = float.TryParse(parts[2], NumberStyles.Float, cultureInfo, out n);
                            if (!success) throw new ArgumentException("Невозможно считать нормальный параметр плоскости как float в первой группе");
                            face.Vertex.X = v - 1;
                            face.Texture.X = t - 1;
                            face.Normal.X = n - 1;

                            parts = words[2].Split('/');
                            success = float.TryParse(parts[0], NumberStyles.Float, cultureInfo, out v);
                            if (!success) throw new ArgumentException("Невозможно считать вершинный параметр плоскости как float во второй группе");
                            success = float.TryParse(parts[1], NumberStyles.Float, cultureInfo, out t);
                            if (!success) throw new ArgumentException("Невозможно считать текстурный параметр плоскости как float во второй группе");
                            success = float.TryParse(parts[2], NumberStyles.Float, cultureInfo, out n);
                            if (!success) throw new ArgumentException("Невозможно считать нормальный параметр плоскости как float во второй группе");
                            face.Vertex.Y = v - 1;
                            face.Texture.Y = t - 1;
                            face.Normal.Y = n - 1;

                            parts = words[3].Split('/');
                            success = float.TryParse(parts[0], NumberStyles.Float, cultureInfo, out v);
                            if (!success) throw new ArgumentException("Невозможно считать вершинный параметр плоскости как float в третьей группе");
                            success = float.TryParse(parts[1], NumberStyles.Float, cultureInfo, out t);
                            if (!success) throw new ArgumentException("Невозможно считать текстурный параметр плоскости как float в третьей группе");
                            success = float.TryParse(parts[2], NumberStyles.Float, cultureInfo, out n);
                            if (!success) throw new ArgumentException("Невозможно считать нормальный параметр плоскости как float в третьей группе");
                            face.Vertex.Z = v - 1;
                            face.Texture.Z = t - 1;
                            face.Normal.Z = n - 1;

                            if (face.Type == FaceTypes.Rectangle)
                            {
                                parts = words[4].Split('/');
                                success = float.TryParse(parts[0], NumberStyles.Float, cultureInfo, out v);
                                if (!success) throw new ArgumentException("Невозможно считать вершинный параметр плоскости как float в четвертой группе");
                                success = float.TryParse(parts[1], NumberStyles.Float, cultureInfo, out t);
                                if (!success) throw new ArgumentException("Невозможно считать текстурный параметр плоскости как float в четвертой группе");
                                success = float.TryParse(parts[2], NumberStyles.Float, cultureInfo, out n);
                                if (!success) throw new ArgumentException("Невозможно считать нормальный параметр плоскости как float в четвертой группе");
                                face.Vertex.W = v - 1;
                                face.Texture.W = t - 1;
                                face.Normal.W = n - 1;
                            }

                            FaceList.Add(face);
                            break;
                            #endregion
                    }
                }
            }
            objStreamReader.Close();
            fileInfo = null;
        }

        private void ParseMTL(string path)
        {
            FileInfo fileInfo = new FileInfo(path);
            mtlStreamReader = fileInfo.OpenText();
            string currentLine = string.Empty;
            List<int> materialPosition = new List<int>();
            MTL mtl = null;
            while ((currentLine = mtlStreamReader.ReadLine()) != null)
            {
                currentLine = new Regex("\\s+").Replace(currentLine, " ");
                string[] words = currentLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (words.Length > 0)
                {
                    float x, y, z;
                    bool success;
                    switch (words[0])
                    {
                        #region NEWMTL
                        case "newmtl":
                            if (mtl != null) { MTL.Add(mtl); }
                            mtl = new MTL();
                            mtl.Name = words[1];
                            break;
                        #endregion
                        #region KD
                        case "Kd":
                            success = float.TryParse(words[1], NumberStyles.Float, cultureInfo, out x);
                            if (!success) throw new ArgumentException("Невозможно считать X параметр материала как float");
                            success = float.TryParse(words[2], NumberStyles.Float, cultureInfo, out y);
                            if (!success) throw new ArgumentException("Невозможно считать Y параметр материала как float");
                            success = float.TryParse(words[3], NumberStyles.Float, cultureInfo, out z);
                            if (!success) throw new ArgumentException("Невозможно считать Z параметр материала как float");
                            mtl.Diffuse = new Vector3(x, y, z);
                            break;
                        #endregion
                        #region KA
                        case "Ka":
                            success = float.TryParse(words[1], NumberStyles.Float, cultureInfo, out x);
                            if (!success) throw new ArgumentException("Невозможно считать X параметр материала как float");
                            success = float.TryParse(words[2], NumberStyles.Float, cultureInfo, out y);
                            if (!success) throw new ArgumentException("Невозможно считать Y параметр материала как float");
                            success = float.TryParse(words[3], NumberStyles.Float, cultureInfo, out z);
                            if (!success) throw new ArgumentException("Невозможно считать Z параметр материала как float");
                            mtl.Ambient = new Vector3(x, y, z);
                            break;
                        #endregion
                        #region D
                        case "d":
                            success = float.TryParse(words[1], NumberStyles.Float, cultureInfo, out x);
                            if (!success) throw new ArgumentException("Невозможно считать X параметр материала как float");
                            mtl.alpha = x;
                            break;
                        #endregion
                        #region MAPKD
                        case "map_Kd":
                            mtl.Texture_str = FolderPath + words[1];
                            mtl.GenerateTexture(device);
                            break;
                            #endregion
                    }
                }
            }
            MTL.Add(mtl);
            mtlStreamReader.Close();
            fileInfo = null;
        }

        private void CalculateVerts()
        {
            if (FaceList[0].Type == FaceTypes.Rectangle) { PrimitivesCount = 4; } else { PrimitivesCount = 3; }
            MassiveObjectsList = new List<MassiveWithTexture>();
            foreach (var mtl in MTL)
            {
                List<CustomVertex.PositionNormalTextured> VertsToAdd = new List<CustomVertex.PositionNormalTextured>();
                foreach (var face in FaceList.Where(f => f.MaterialName == mtl.Name))
                {
                    CustomVertex.PositionNormalTextured[] polygon = new CustomVertex.PositionNormalTextured[PrimitivesCount];
                    polygon[0] = new CustomVertex.PositionNormalTextured(
                            VertexList[(int)face.Vertex.X].X,
                            VertexList[(int)face.Vertex.X].Y,
                            VertexList[(int)face.Vertex.X].Z,
                            NormalList[(int)face.Normal.X].X,
                            NormalList[(int)face.Normal.X].Y,
                            NormalList[(int)face.Normal.X].Z,
                            TextureList[(int)face.Texture.X].X,
                            TextureList[(int)face.Texture.X].Y
                        );
                    VertsToAdd.Add(polygon[0]);
                    polygon[1] = new CustomVertex.PositionNormalTextured(
                        VertexList[(int)face.Vertex.Y].X,
                        VertexList[(int)face.Vertex.Y].Y,
                        VertexList[(int)face.Vertex.Y].Z,
                        NormalList[(int)face.Normal.Y].X,
                        NormalList[(int)face.Normal.Y].Y,
                        NormalList[(int)face.Normal.Y].Z,
                        TextureList[(int)face.Texture.Y].X,
                        TextureList[(int)face.Texture.Y].Y
                    );
                    VertsToAdd.Add(polygon[1]);
                    polygon[2] = new CustomVertex.PositionNormalTextured(
                        VertexList[(int)face.Vertex.Z].X,
                        VertexList[(int)face.Vertex.Z].Y,
                        VertexList[(int)face.Vertex.Z].Z,
                        NormalList[(int)face.Normal.Z].X,
                        NormalList[(int)face.Normal.Z].Y,
                        NormalList[(int)face.Normal.Z].Z,
                        TextureList[(int)face.Texture.Z].X,
                        TextureList[(int)face.Texture.Z].Y
                    );
                    VertsToAdd.Add(polygon[2]);
                    if (FaceList[0].Type == FaceTypes.Rectangle)
                    {
                        polygon[3] = new CustomVertex.PositionNormalTextured(
                            VertexList[(int)face.Vertex.W].X,
                            VertexList[(int)face.Vertex.W].Y,
                            VertexList[(int)face.Vertex.W].Z,
                            NormalList[(int)face.Normal.W].X,
                            NormalList[(int)face.Normal.W].Y,
                            NormalList[(int)face.Normal.W].Z,
                            TextureList[(int)face.Texture.W].X,
                            TextureList[(int)face.Texture.W].Y
                        );
                        VertsToAdd.Add(polygon[3]);
                    }
                }

                MassiveWithTexture massive = new MassiveWithTexture();
                massive.massive = VertsToAdd.ToArray();
                massive.texture = mtl.RealTexture;
                massive.material = mtl.Name;
                MassiveObjectsList.Add(massive);
            }
        }

        public void Draw()
        {
            device.VertexFormat = CustomVertex.PositionNormalTextured.Format;

            foreach (var partOfObject in MassiveObjectsList)
            {
                device.SetTexture(0, partOfObject.texture);
                if (partOfObject.material == "Eye")
                {
                    for (int i = 0; i < partOfObject.massive.Count(); i++)
                    {
                        partOfObject.massive[i].Tu += Emotion_x;
                        partOfObject.massive[i].Tv += Emotion_y;
                    }
                }
                if (FaceList[0].Type == FaceTypes.Rectangle)
                { device.DrawUserPrimitives(PrimitiveType.TriangleFan, partOfObject.massive.Count() / PrimitivesCount, partOfObject.massive); }
                else { device.DrawUserPrimitives(PrimitiveType.TriangleList, partOfObject.massive.Count() / PrimitivesCount, partOfObject.massive); }
                device.SetTexture(0, null);
                if (partOfObject.material == "Eye")
                {
                    for (int i = 0; i < partOfObject.massive.Count(); i++)
                    {
                        partOfObject.massive[i].Tu -= Emotion_x;
                        partOfObject.massive[i].Tv -= Emotion_y;
                    }
                }
            }
        }
    }

    public class Face
    {
        public Vector4 Vertex;
        public Vector4 Texture;
        public Vector4 Normal;
        public FaceTypes Type;
        public string MaterialName;
        public Face() { }
    }

    public class MTL
    {
        public string Name;
        public Vector3 Diffuse;
        public Vector3 Ambient;
        public float alpha;
        public string Texture_str;
        public Texture RealTexture;
        public MTL() { }
        public void GenerateTexture(Device device)
        {
            Bitmap bitmap = new Bitmap(Texture_str);
            RealTexture = new Texture(device, bitmap, Usage.Dynamic | Usage.AutoGenerateMipMap, Pool.Default);
        }
    }

    public enum FaceTypes { Triangle, Rectangle }
}