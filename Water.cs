﻿using System.Collections.Generic;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using System.Drawing;
using System;
namespace Benihana
{
    class Water
    {
        private Device device;
        private Bitmap WaterMap, Water1, Water2, Water3;
        private Texture MainTexture, WaterTexture1, WaterTexture2, WaterTexture3, WaterTexture4;
        private CustomVertex.PositionNormalTextured[] Verts;
        private List<Vector3> PositionList;
        private Vector3 TargetPosition, CameraDirection;
        public Water(Device device)
        {
            this.device = device;
            WaterMap = new Bitmap(@"Landscapes\Level1\Level1WM.png");
            Water1 = new Bitmap(@"Landscapes\Level1\Level1W1.png");
            Water2 = new Bitmap(@"Landscapes\Level1\Level1W2.png");
            Water3 = new Bitmap(@"Landscapes\Level1\Level1W3.png");
            WaterTexture1 = new Texture(device, Water1, Usage.Dynamic | Usage.AutoGenerateMipMap, Pool.Default);
            WaterTexture2 = new Texture(device, Water2, Usage.Dynamic | Usage.AutoGenerateMipMap, Pool.Default);
            WaterTexture3 = new Texture(device, Water3, Usage.Dynamic | Usage.AutoGenerateMipMap, Pool.Default);
            WaterTexture4 = new Texture(device, Water2, Usage.Dynamic | Usage.AutoGenerateMipMap, Pool.Default);
            MainTexture = WaterTexture1;
            BuildPolygone();
            Load();
        }
        public void ResetDevice(Device device)
        {
            this.device = device;
            WaterTexture1 = new Texture(device, Water1, Usage.Dynamic | Usage.AutoGenerateMipMap, Pool.Default);
            WaterTexture2 = new Texture(device, Water2, Usage.Dynamic | Usage.AutoGenerateMipMap, Pool.Default);
            WaterTexture3 = new Texture(device, Water3, Usage.Dynamic | Usage.AutoGenerateMipMap, Pool.Default);
            WaterTexture4 = new Texture(device, Water2, Usage.Dynamic | Usage.AutoGenerateMipMap, Pool.Default);
            MainTexture = WaterTexture1;
        }
        private void BuildPolygone()
        {
            float half = 0.5f;
            Verts = new CustomVertex.PositionNormalTextured[6];

            Verts[0] = new CustomVertex.PositionNormalTextured(-half, 0, -half,     0, 1, 0,    0, 0);
            Verts[1] = new CustomVertex.PositionNormalTextured(-half, 0, +half,     0, 1, 0,    0, 1);
            Verts[2] = new CustomVertex.PositionNormalTextured(+half, 0, +half,     0, 1, 0,    1, 1);

            Verts[3] = new CustomVertex.PositionNormalTextured(-half, 0, -half,     0, 1, 0,    0, 0);
            Verts[4] = new CustomVertex.PositionNormalTextured(+half, 0, +half,     0, 1, 0,    1, 1);
            Verts[5] = new CustomVertex.PositionNormalTextured(+half, 0, -half,     0, 1, 0,    1, 0);
        }
        public void Load()
        {
            PositionList = new List<Vector3>();
            for (int z = 0; z < WaterMap.Height; z++)
            {
                for (int x = 0; x < WaterMap.Width; x++)
                {
                    if (WaterMap.GetPixel(x, z) == Color.FromArgb(0, 0, 255))
                    {
                        PositionList.Add(new Vector3(x, 0.2f, z));
                    }
                }
            }
        }
        public void Update(Vector3 Position, Vector3 Direction)
        {
            TargetPosition = Position;
            CameraDirection = Direction;
        }
        int frameCounter = 0;
        public void Draw()
        {
            Material material = new Material();
            material.Diffuse = Color.White;
            material.Ambient = Color.Gray;
            material.Specular = Color.White;
            device.Material = material;

            device.VertexFormat = CustomVertex.PositionNormalTextured.Format;
            device.SetTexture(0, MainTexture);

            foreach (var position in PositionList)
            {
                Vector2 v2cam = new Vector2(TargetPosition.X, TargetPosition.Z);
                Vector2 v2grs = new Vector2(position.X, position.Z);
                Vector2 v2dist = v2cam - v2grs;
                
                if (v2dist.Length() < Program.DrawDistance)
                {
                    Vector2 v2dir = new Vector2(CameraDirection.X, CameraDirection.Z);
                    float viewAngle = Program.RadianToDegree(Program.AngleBeetwean(v2dir, v2dist));
                    if (viewAngle > 180 - 45 && viewAngle < 180 + 45)
                    {
                        float grassAngle = Program.AngleBeetwean(new Vector2(0, 1), v2dist);
                        device.Transform.World = Matrix.Identity;
                        device.Transform.World = Matrix.Translation(position);
                        device.DrawUserPrimitives(PrimitiveType.TriangleList, 2, Verts);
                    }

                    if (v2dist.Length() < 5 && frameCounter > 8)
                    {
                        if (MainTexture == WaterTexture1) { MainTexture = WaterTexture2; }
                        else if (MainTexture == WaterTexture2) { MainTexture = WaterTexture3; }
                        else if (MainTexture == WaterTexture3) { MainTexture = WaterTexture4; }
                        else { MainTexture = WaterTexture1; }
                        frameCounter = 0;
                    }
                }
            }
            device.SetTexture(0, null);
            device.Transform.World = Matrix.Identity;

            frameCounter++;
        }
    }
}