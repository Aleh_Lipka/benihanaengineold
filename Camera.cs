﻿using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
namespace Benihana
{
    enum Axis { X, Y, Z };
    class Camera
    {
        private Device device;
        private DXPanel dxPanel;
        private Vector3 position;
        public Vector3 direction;
        public Vector3 up;
        private float near = 1.0f;
        private float far = 1000.0f;
        public Camera(Device device, DXPanel dxPanel, Vector3 position)
        {
            this.device = device;
            this.dxPanel = dxPanel;
            //device.RenderState.FillMode = FillMode.WireFrame;
            device.RenderState.CullMode = Cull.Clockwise;
            device.RenderState.Lighting = true;
            this.position = position;
            direction = new Vector3(0, 0, -5);
            up = new Vector3(0.0f, 1.0f, 0.0f);
            device.Transform.Projection = Matrix.PerspectiveFovRH(Geometry.DegreeToRadian(45.0f), (float)dxPanel.Width / dxPanel.Height, near, far);
            Update();
        }
        public void Update()
        {
            device.Transform.View = Matrix.LookAtRH(position, position + direction, up);
        }
        public void ResetDevice(Device device)
        {
            this.device = device;
            //device.RenderState.FillMode = FillMode.WireFrame;
            device.RenderState.CullMode = Cull.Clockwise;
            device.RenderState.Lighting = true;
            device.Transform.Projection = Matrix.PerspectiveFovRH(Geometry.DegreeToRadian(45.0f), (float)dxPanel.Width / dxPanel.Height, near, far);
            Update();
        }
        #region - SET & GET OPTIONS -
        /// <summary>
        /// Задает позицию камеры
        /// </summary>
        /// <param name="position">Вектор позиции камеры. По-умолчанию: new Vector3(0.0f, 0.0f, 0.0f)</param>
        /// <param name="increment">Если параметр равен true значение будет сложено с текущим</param>
        public void SetPosition(Vector3 position, bool increment = false)
        {
            if (increment) { this.position += position; }
            else { this.position = position; }
            Update();
        }
        /// <summary>
        /// Задает цель камеры
        /// </summary>
        /// <param name="target">Вектор позиции цели. По-умолчанию: new Vector3(0.0f, 0.0f, 0.0f)</param>
        /// <param name="increment">Если параметр равен true значение будет сложено с текущим</param>
        public void SetTarget(Vector3 target, bool increment = false)
        {
            if (increment) { this.direction += target; }
            else { this.direction = target; }
            Update();
        }
        /// <summary>
        /// Задет вектор высоты для камеры
        /// </summary>
        /// <param name="up">Вектор высоты камеры. По-умолчанию: new Vector3(0.0f, 1.0f, 0.0f)</param>
        public void SetUpVector(Vector3 up)
        {
            this.up = up;
            Update();
        }

        /// <summary>
        /// Задает ближнюю плоскость отсечения
        /// </summary>
        /// <param name="nearPlane">Значение. По-умолчанию 0.01f</param>
        public void SetNearPlane(float nearPlane)
        {
            near = nearPlane;
            Update();
        }
        /// <summary>
        /// Задает дальнюю плоскость отсечения
        /// </summary>
        /// <param name="farPlane">Значение. По-умолчанию 10000.0f</param>
        public void SetFarPlane(float farPlane)
        {
            far = farPlane;
            Update();
        }

        /// <summary>
        /// Возвращает положение камеры
        /// </summary>
        public Vector3 GetPosition()
        {
            return position;
        }
        /// <summary>
        /// Возвращает положение цели камеры
        /// </summary>
        public Vector3 GetTarget()
        {
            return direction;
        }
        /// <summary>
        /// Возвращает Up-вектор камеры
        /// </summary>
        public Vector3 GetUpVector()
        {
            return up;
        }

        /// <summary>
        /// Возвращает значение передней плоскости отсечения
        /// </summary>
        public float GetNearPlane()
        {
            return near;
        }
        /// <summary>
        /// Возвращает значение задней плоскости отсечения
        /// </summary>
        public float GetFarPlane()
        {
            return far;
        }
        #endregion
    }
}