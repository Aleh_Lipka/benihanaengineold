﻿using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using System.Drawing;
namespace Benihana
{
    class SkyBox
    {
        private Device device;
        private CustomVertex.PositionTextured[] Verts;
        private Bitmap TextureMap;
        private Texture MainTexture;
        public SkyBox(Device device)
        {
            this.device = device;
            TextureMap = new Bitmap(@"Landscapes\Level1\Level1SB.jpg");
            MainTexture = new Texture(device, TextureMap, Usage.Dynamic, Pool.Default);
        }
        public void Update(Vector3 cameraPosition)
        {
            float x = cameraPosition.X;
            float y = cameraPosition.Y;
            float z = cameraPosition.Z;
            float step = 100f;

            Verts = new CustomVertex.PositionTextured[36];
            // FRONT
            Verts[0] = new CustomVertex.PositionTextured(x - step, y + step, z - step, 0.25f, 1.0f/3);
            Verts[1] = new CustomVertex.PositionTextured(x - step, y - step, z - step, 0.25f, 1.0f - 1.0f/3);
            Verts[2] = new CustomVertex.PositionTextured(x + step, y + step, z - step, 0.5f, 1.0f/3);
            Verts[3] = new CustomVertex.PositionTextured(x - step, y - step, z - step, 0.25f, 1.0f - 1.0f/3);
            Verts[4] = new CustomVertex.PositionTextured(x + step, y - step, z - step, 0.5f, 1.0f - 1.0f/3);
            Verts[5] = new CustomVertex.PositionTextured(x + step, y + step, z - step, 0.5f, 1.0f/3);
            // LEFT
            Verts[6] = new CustomVertex.PositionTextured(x - step, y + step, z + step, 0, 1.0f/3);
            Verts[7] = new CustomVertex.PositionTextured(x - step, y - step, z + step, 0, 1.0f - 1.0f/3);
            Verts[8] = new CustomVertex.PositionTextured(x - step, y + step, z - step, 0.25f, 1.0f/3);
            Verts[9] = new CustomVertex.PositionTextured(x - step, y - step, z + step, 0, 1.0f - 1.0f/3);
            Verts[10] = new CustomVertex.PositionTextured(x - step, y - step, z - step, 0.25f, 1.0f - 1.0f/3);
            Verts[11] = new CustomVertex.PositionTextured(x - step, y + step, z - step, 0.25f, 1.0f/3);
            // RIGHT
            Verts[12] = new CustomVertex.PositionTextured(x + step, y + step, z - step, 0.5f, 1.0f/3);
            Verts[13] = new CustomVertex.PositionTextured(x + step, y - step, z - step, 0.5f, 1.0f - 1.0f/3);
            Verts[14] = new CustomVertex.PositionTextured(x + step, y + step, z + step, 0.75f, 1.0f/3);
            Verts[15] = new CustomVertex.PositionTextured(x + step, y - step, z - step, 0.5f, 1.0f - 1.0f/3);
            Verts[16] = new CustomVertex.PositionTextured(x + step, y - step, z + step, 0.75f, 1.0f - 1.0f/3);
            Verts[17] = new CustomVertex.PositionTextured(x + step, y + step, z + step, 0.75f, 1.0f/3);
            // BACK
            Verts[18] = new CustomVertex.PositionTextured(x + step, y + step, z + step, 0.75f, 1.0f/3);
            Verts[19] = new CustomVertex.PositionTextured(x + step, y - step, z + step, 0.75f, 1.0f - 1.0f/3);
            Verts[20] = new CustomVertex.PositionTextured(x - step, y + step, z + step, 1, 1.0f/3);
            Verts[21] = new CustomVertex.PositionTextured(x + step, y - step, z + step, 0.75f, 1.0f - 1.0f/3);
            Verts[22] = new CustomVertex.PositionTextured(x - step, y - step, z + step, 1, 1.0f - 1.0f/3);
            Verts[23] = new CustomVertex.PositionTextured(x - step, y + step, z + step, 1, 1.0f/3);
            // UP
            Verts[24] = new CustomVertex.PositionTextured(x - step, y + step, z + step, 0.25f, 0);
            Verts[25] = new CustomVertex.PositionTextured(x - step, y + step, z - step, 0.25f, 1.0f/3);
            Verts[26] = new CustomVertex.PositionTextured(x + step, y + step, z + step, 0.5f, 0);
            Verts[27] = new CustomVertex.PositionTextured(x - step, y + step, z - step, 0.25f, 1.0f/3);
            Verts[28] = new CustomVertex.PositionTextured(x + step, y + step, z - step, 0.5f, 1.0f/3);
            Verts[29] = new CustomVertex.PositionTextured(x + step, y + step, z + step, 0.5f, 0);
            // DOWN
            Verts[30] = new CustomVertex.PositionTextured(x - step, y - step, z - step, 0.25f, 1.0f - 1.0f/3);
            Verts[31] = new CustomVertex.PositionTextured(x - step, y - step, z + step, 0.25f, 1);
            Verts[32] = new CustomVertex.PositionTextured(x + step, y - step, z - step, 0.5f, 1.0f - 1.0f/3);
            Verts[33] = new CustomVertex.PositionTextured(x - step, y - step, z + step, 0.25f, 1);
            Verts[34] = new CustomVertex.PositionTextured(x + step, y - step, z + step, 0.5f, 1);
            Verts[35] = new CustomVertex.PositionTextured(x + step, y - step, z - step, 0.5f, 1.0f - 1.0f/3);
        }
        public void ResetDevice(Device device)
        {
            this.device = device;
            MainTexture = new Texture(device, TextureMap, Usage.Dynamic, Pool.Default);
        }
        public void Draw()
        {
            Material material = new Material();
            material.Diffuse = Color.White;
            material.Ambient = Color.White;
            device.Material = material;

            device.Transform.World = Matrix.Identity;
            device.VertexFormat = CustomVertex.PositionTextured.Format;
            device.RenderState.ZBufferEnable = false;
            device.RenderState.FogEnable = false;
            device.SetTexture(0, MainTexture);
            device.DrawUserPrimitives(PrimitiveType.TriangleList, 12, Verts);
            device.SetTexture(0, null);
            device.RenderState.FogEnable = true;
            device.RenderState.ZBufferEnable = true;
            device.Transform.World = Matrix.Identity;
        }
    }
}