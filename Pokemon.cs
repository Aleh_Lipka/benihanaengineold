﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using Benihana.Parser.OBJ;
using System.Threading;
using System.Drawing;
namespace Benihana
{
    class Pokemon
    {
        public int Health;
        public int Hungry;
        public int Fun;
        public int Power;
        public string Name;
        public bool IsFree;
        public string TrainerName;
        public Program.PokemonType Type;
        public bool IsDead;
        public int Age;
        public Program.Gender Gender;
        public bool Enabled;
        public Program.Emotion Emotion;
        public Vector3 Position;
        private string ModelPath;
        private Device device;
        private OBJParser parser;
        private Timer timer;
        private Vector3 CameraPosition, CameraDirection;
        public Pokemon(Device device, string name)
        {
            this.device = device;
            Name = name;
            DefaultParametres();
            ModelPath = $"Models\\Pokemons\\{Name}\\{Name}.objmodel";
            parser = new OBJParser(device);
            timer = new Timer();
            Load();
        }
        public void Load()
        {
            parser.Read(ModelPath, 40);
        }
        public void ResetDevice(Device device)
        {
            this.device = device;
            parser.ResetDevice(this.device);
        }
        public void DefaultParametres()
        {
            Health = 100;
            Hungry = 0;
            Fun = 100;
            Power = 100;
            IsFree = true;
            TrainerName = string.Empty;
            Type = Program.PokemonType.Ground;
            IsDead = false;
            Age = 1;
            Gender = Program.Gender.Male;
            Enabled = true;
            Emotion = Program.Emotion.A;
            Position = Vector3.Empty;
        }
        private void EyeUpdate()
        {
            if (Emotion == Program.Emotion.A) { parser.Emotion_x = 0.0f; parser.Emotion_y = 0.00f; }
            if (Emotion == Program.Emotion.B) { parser.Emotion_x = 0.0f; parser.Emotion_y = 0.25f; }
            if (Emotion == Program.Emotion.C) { parser.Emotion_x = 0.0f; parser.Emotion_y = 0.50f; }
            if (Emotion == Program.Emotion.D) { parser.Emotion_x = 0.0f; parser.Emotion_y = 0.75f; }
            if (Emotion == Program.Emotion.E) { parser.Emotion_x = 0.5f; parser.Emotion_y = 0.00f; }
            if (Emotion == Program.Emotion.F) { parser.Emotion_x = 0.5f; parser.Emotion_y = 0.25f; }
            if (Emotion == Program.Emotion.G) { parser.Emotion_x = 0.5f; parser.Emotion_y = 0.50f; }
        }
        public void Update(Vector3 Position, Vector3 Direction)
        {
            CameraPosition = Position;
            CameraDirection = Direction;
            EyeUpdate();
        }
        private void EyeCloseOpen()
        {
            int speed = 60;
            int wait = 9000;

            if (timer.CurrentValue() == 0) timer.Start();
            else if (timer.CurrentValue() < speed) Emotion = Program.Emotion.B;
            else if (timer.CurrentValue() > speed && timer.CurrentValue() < speed * 2) Emotion = Program.Emotion.C;
            else if (timer.CurrentValue() > speed * 2 && timer.CurrentValue() < speed * 3) Emotion = Program.Emotion.B;
            else if (timer.CurrentValue() > speed * 3 && timer.CurrentValue() < speed * 4) Emotion = Program.Emotion.A;
            else if (timer.CurrentValue() > wait) timer.Stop();
            EyeUpdate();
        }
        public void Draw()
        {
            Material material = new Material();
            material.Diffuse = Color.White;
            material.Ambient = Color.LightGray;
            material.Specular = Color.White;
            device.Material = material;

            Vector2 v2cam = new Vector2(CameraPosition.X, CameraPosition.Z);
            Vector2 v2grs = new Vector2(Position.X, Position.Z);
            Vector2 v2dist = v2cam - v2grs;
            if (v2dist.Length() < Program.DrawDistance)
            {
                Vector2 v2dir = new Vector2(CameraDirection.X, CameraDirection.Z);
                float viewAngle = Program.RadianToDegree(Program.AngleBeetwean(v2dir, v2dist));
                if (viewAngle > 180 - 45 && viewAngle < 180 + 45)
                {
                    EyeCloseOpen();
                    device.Transform.World = Matrix.Identity;
                    device.Transform.World = Matrix.Translation(Position);
                    parser.Draw();
                }
            }
            device.Transform.World = Matrix.Identity;
        }
    }
}